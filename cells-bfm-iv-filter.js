{
  const {
    html,
  } = Polymer;
  /**
    `<cells-bfm-iv-filter>` Description.

    Example:

    ```html
    <cells-bfm-iv-filter></cells-bfm-iv-filter>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-bfm-iv-filter | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsBfmIvFilter extends Polymer.Element {

    static get is() {
      return 'cells-bfm-iv-filter';
    }

    static get properties() {
      return {
        title: String,
        filter: {
          type: Array,
          value: () => ([])
        },
        actions: {
          type: Array,
          value: () => ([])
        },
        data: {
          type: Object,
          value: () => ({})
        },
      };
    }
    /**
     * Function to save date from inputs
     * @param {*} e
     */
    _handleInput(e) {
      this.set(`data.${e.target.name}`, e.detail.value);
    }

    /**
     * Function to send custom event and validate if detail require data saved from inputs
     * @param {*} e
     */
    _handleAction(e) {
      let data = (e.model.__data.item.hasParams) ? this.data : {};
      this.dispatchEvent(new CustomEvent(e.model.__data.item.event, {detail: data}));
    }

  }

  customElements.define(CellsBfmIvFilter.is, CellsBfmIvFilter);
}